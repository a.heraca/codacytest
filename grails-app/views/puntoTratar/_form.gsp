<%@ page import="ControlDeReuniones.PuntoTratar" %>



<div class="fieldcontain ${hasErrors(bean: puntoTratarInstance, field: 'minuta', 'error')} required">
	<label for="minuta">
		<g:message code="puntoTratar.minuta.label" default="Minuta" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="minuta" name="minuta.id" from="${ControlDeReuniones.Minuta.list()}" optionKey="id" required="" value="${puntoTratarInstance?.minuta?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: puntoTratarInstance, field: 'puntoTratar', 'error')} ">
	<label for="puntoTratar">
		<g:message code="puntoTratar.puntoTratar.label" default="Punto Tratar" />
		
	</label>
	<g:textField name="puntoTratar" value="${puntoTratarInstance?.puntoTratar}"/>
</div>

