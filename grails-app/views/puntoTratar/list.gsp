
<%@ page import="ControlDeReuniones.PuntoTratar" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'puntoTratar.label', default: 'PuntoTratar')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-puntoTratar" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-puntoTratar" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<th><g:message code="puntoTratar.minuta.label" default="Minuta" /></th>
					
						<g:sortableColumn property="puntoTratar" title="${message(code: 'puntoTratar.puntoTratar.label', default: 'Punto Tratar')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${puntoTratarInstanceList}" status="i" var="puntoTratarInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${puntoTratarInstance.id}">${fieldValue(bean: puntoTratarInstance, field: "minuta")}</g:link></td>
					
						<td>${fieldValue(bean: puntoTratarInstance, field: "puntoTratar")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${puntoTratarInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
