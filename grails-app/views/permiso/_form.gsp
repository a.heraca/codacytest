<%@ page import="ControlDeReuniones.Permiso" %>



<div class="fieldcontain ${hasErrors(bean: permisoInstance, field: 'url', 'error')} required">
	<label for="url">
		<g:message code="permiso.url.label" default="Url" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="url" required="" value="${permisoInstance?.url}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: permisoInstance, field: 'configAttribute', 'error')} required">
	<label for="configAttribute">
		<g:message code="permiso.configAttribute.label" default="Config Attribute" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="configAttribute" required="" value="${permisoInstance?.configAttribute}"/>
</div>

