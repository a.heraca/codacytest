<%@ page import="ControlDeReuniones.Minuta" %>



<div class="fieldcontain ${hasErrors(bean: minutaInstance, field: 'identificador', 'error')} required">
	<label for="identificador">
		<g:message code="minuta.identificador.label" default="Identificador" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="identificador" required="" value="${minutaInstance?.identificador}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: minutaInstance, field: 'acuerdo', 'error')} ">
	<label for="acuerdo">
		<g:message code="minuta.acuerdo.label" default="Acuerdo" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${minutaInstance?.acuerdo?}" var="a">
    <li><g:link controller="acuerdo" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="acuerdo" action="create" params="['minuta.id': minutaInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'acuerdo.label', default: 'Acuerdo')])}</g:link>
</li>
</ul>

</div>

<div class="fieldcontain ${hasErrors(bean: minutaInstance, field: 'estadoMinuta', 'error')} ">
	<label for="estadoMinuta">
		<g:message code="minuta.estadoMinuta.label" default="Estado Minuta" />
		
	</label>
	<g:textField name="estadoMinuta" value="${minutaInstance?.estadoMinuta}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: minutaInstance, field: 'fechaFin', 'error')} required">
	<label for="fechaFin">
		<g:message code="minuta.fechaFin.label" default="Fecha Fin" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="fechaFin" precision="day"  value="${minutaInstance?.fechaFin}"  />
</div>

<div class="fieldcontain ${hasErrors(bean: minutaInstance, field: 'fechaInicio', 'error')} required">
	<label for="fechaInicio">
		<g:message code="minuta.fechaInicio.label" default="Fecha Inicio" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="fechaInicio" precision="day"  value="${minutaInstance?.fechaInicio}"  />
</div>

<div class="fieldcontain ${hasErrors(bean: minutaInstance, field: 'horaFin', 'error')} ">
	<label for="horaFin">
		<g:message code="minuta.horaFin.label" default="Hora Fin" />
		
	</label>
	<g:textField name="horaFin" value="${minutaInstance?.horaFin}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: minutaInstance, field: 'horaInicio', 'error')} ">
	<label for="horaInicio">
		<g:message code="minuta.horaInicio.label" default="Hora Inicio" />
		
	</label>
	<g:textField name="horaInicio" value="${minutaInstance?.horaInicio}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: minutaInstance, field: 'lugar', 'error')} ">
	<label for="lugar">
		<g:message code="minuta.lugar.label" default="Lugar" />
		
	</label>
	<g:textField name="lugar" value="${minutaInstance?.lugar}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: minutaInstance, field: 'objetivo', 'error')} ">
	<label for="objetivo">
		<g:message code="minuta.objetivo.label" default="Objetivo" />
		
	</label>
	<g:textField name="objetivo" value="${minutaInstance?.objetivo}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: minutaInstance, field: 'puntoTratar', 'error')} ">
	<label for="puntoTratar">
		<g:message code="minuta.puntoTratar.label" default="Punto Tratar" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${minutaInstance?.puntoTratar?}" var="p">
    <li><g:link controller="puntoTratar" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="puntoTratar" action="create" params="['minuta.id': minutaInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'puntoTratar.label', default: 'PuntoTratar')])}</g:link>
</li>
</ul>

</div>

<div class="fieldcontain ${hasErrors(bean: minutaInstance, field: 'tipoReunion', 'error')} required">
	<label for="tipoReunion">
		<g:message code="minuta.tipoReunion.label" default="Tipo Reunion" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="tipoReunion" name="tipoReunion.id" from="${ControlDeReuniones.TipoReunion.list()}" optionKey="id" required="" value="${minutaInstance?.tipoReunion?.id}" class="many-to-one"/>
</div>



<div class="fieldcontain ${hasErrors(bean: minutaInstance, field: 'usuario', 'error')} ">
	<label for="usuario">
		<g:message code="minuta.usuario.label" default="Usuario" />
		
	</label>
	<g:select name="usuario" from="${ControlDeReuniones.Usuario.list()}" multiple="multiple" optionKey="id" size="5" value="${minutaInstance?.usuario*.id}" class="many-to-many"/>
</div>

