
<%@ page import="ControlDeReuniones.Acuerdo" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'acuerdo.label', default: 'Acuerdo')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-acuerdo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-acuerdo" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list acuerdo">
			
				<g:if test="${acuerdoInstance?.estado}">
				<li class="fieldcontain">
					<span id="estado-label" class="property-label"><g:message code="acuerdo.estado.label" default="Estado" /></span>
					
						<span class="property-value" aria-labelledby="estado-label"><g:fieldValue bean="${acuerdoInstance}" field="estado"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${acuerdoInstance?.descripcionAcuerdo}">
				<li class="fieldcontain">
					<span id="descripcionAcuerdo-label" class="property-label"><g:message code="acuerdo.descripcionAcuerdo.label" default="Descripcion Acuerdo" /></span>
					
						<span class="property-value" aria-labelledby="descripcionAcuerdo-label"><g:fieldValue bean="${acuerdoInstance}" field="descripcionAcuerdo"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${acuerdoInstance?.fechaCompromiso}">
				<li class="fieldcontain">
					<span id="fechaCompromiso-label" class="property-label"><g:message code="acuerdo.fechaCompromiso.label" default="Fecha Compromiso" /></span>
					
						<span class="property-value" aria-labelledby="fechaCompromiso-label"><g:formatDate date="${acuerdoInstance?.fechaCompromiso}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${acuerdoInstance?.minuta}">
				<li class="fieldcontain">
					<span id="minuta-label" class="property-label"><g:message code="acuerdo.minuta.label" default="Minuta" /></span>
					
						<span class="property-value" aria-labelledby="minuta-label"><g:link controller="minuta" action="show" id="${acuerdoInstance?.minuta?.id}">${acuerdoInstance?.minuta?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${acuerdoInstance?.usuario}">
				<li class="fieldcontain">
					<span id="usuario-label" class="property-label"><g:message code="acuerdo.usuario.label" default="Usuario" /></span>
					
						<g:each in="${acuerdoInstance.usuario}" var="u">
						<span class="property-value" aria-labelledby="usuario-label"><g:link controller="usuario" action="show" id="${u.id}">${u?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${acuerdoInstance?.id}" />
					<g:link class="edit" action="edit" id="${acuerdoInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
