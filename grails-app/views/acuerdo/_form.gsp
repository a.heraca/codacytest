<%@ page import="ControlDeReuniones.Acuerdo" %>



<div class="fieldcontain ${hasErrors(bean: acuerdoInstance, field: 'estado', 'error')} ">
	<label for="estado">
		<g:message code="acuerdo.estado.label" default="Estado" />
		
	</label>
	<g:select name="estado" from="${acuerdoInstance.constraints.estado.inList}" value="${acuerdoInstance?.estado}" valueMessagePrefix="acuerdo.estado" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: acuerdoInstance, field: 'descripcionAcuerdo', 'error')} ">
	<label for="descripcionAcuerdo">
		<g:message code="acuerdo.descripcionAcuerdo.label" default="Descripcion Acuerdo" />
		
	</label>
	<g:textField name="descripcionAcuerdo" value="${acuerdoInstance?.descripcionAcuerdo}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: acuerdoInstance, field: 'fechaCompromiso', 'error')} required">
	<label for="fechaCompromiso">
		<g:message code="acuerdo.fechaCompromiso.label" default="Fecha Compromiso" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="fechaCompromiso" precision="day"  value="${acuerdoInstance?.fechaCompromiso}"  />
</div>

<div class="fieldcontain ${hasErrors(bean: acuerdoInstance, field: 'minuta', 'error')} required">
	<label for="minuta">
		<g:message code="acuerdo.minuta.label" default="Minuta" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="minuta" name="minuta.id" from="${ControlDeReuniones.Minuta.list()}" optionKey="id" required="" value="${acuerdoInstance?.minuta?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: acuerdoInstance, field: 'usuario', 'error')} ">
	<label for="usuario">
		<g:message code="acuerdo.usuario.label" default="Usuario" />
		
	</label>
	<g:select name="usuario" from="${ControlDeReuniones.Usuario.list()}" multiple="multiple" optionKey="id" size="5" value="${acuerdoInstance?.usuario*.id}" class="many-to-many"/>
</div>

