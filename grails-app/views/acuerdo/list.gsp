
<%@ page import="ControlDeReuniones.Acuerdo" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'acuerdo.label', default: 'Acuerdo')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-acuerdo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-acuerdo" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="estado" title="${message(code: 'acuerdo.estado.label', default: 'Estado')}" />
					
						<g:sortableColumn property="descripcionAcuerdo" title="${message(code: 'acuerdo.descripcionAcuerdo.label', default: 'Descripcion Acuerdo')}" />
					
						<g:sortableColumn property="fechaCompromiso" title="${message(code: 'acuerdo.fechaCompromiso.label', default: 'Fecha Compromiso')}" />
					
						<th><g:message code="acuerdo.minuta.label" default="Minuta" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${acuerdoInstanceList}" status="i" var="acuerdoInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${acuerdoInstance.id}">${fieldValue(bean: acuerdoInstance, field: "estado")}</g:link></td>
					
						<td>${fieldValue(bean: acuerdoInstance, field: "descripcionAcuerdo")}</td>
					
						<td><g:formatDate date="${acuerdoInstance.fechaCompromiso}" /></td>
					
						<td>${fieldValue(bean: acuerdoInstance, field: "minuta")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${acuerdoInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
