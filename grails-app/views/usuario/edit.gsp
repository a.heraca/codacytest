<%@ page import="ControlDeReuniones.Usuario" %>
<!doctype html>
<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <meta name="layout" content="main">
  <g:set var="entityName" value="${message(code: 'usuario.label', default: 'Usuario')}" />
  <title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>
<body>

  <div id="edit-usuario" class="content scaffold-edit" role="main">
    <h2>Editar Usuario</h2>
    <g:if test="${flash.message}">
      <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${usuarioInstance}">
      <ul class="errors" role="alert">
        <g:eachError bean="${usuarioInstance}" var="error">
          <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
        </g:eachError>
      </ul>
    </g:hasErrors>
    <g:form method="post" >
      <g:hiddenField id="id" name="id" value="${usuarioInstance?.id}" />
      <g:hiddenField id="version" name="version" value="${usuarioInstance?.version}" />
      <g:render template="form"/><br/>
      <fieldset class="buttons"  style="width:738px" >   
        <div id="btn" class="mostrar">
          <g:actionSubmit id="boton1" onclick="cam(1)" action="verup" class="save" value="Guardar" /> <input type="button" class="cancel" value="Cancelar" onclick="can()"/>
        </div>
        <div id="btn2" class="ocultar">
          <input type="button" id="boton2" value="Guardar" class="save" onclick="admin2()" /> <input type="button" class="cancel" value="Cancelar" onclick="can()"/>
        </div>

      </fieldset>
    </g:form>
  </div><br/><br/>
</body>
</html>