
<%@ page import="ControlDeReuniones.TipoReunion" %>
<!doctype html>

<meta name="layout" content="main"/>
<g:javascript library="prototype" />
<g:javascript src="JSCon.js"/>
<link rel="stylesheet" href="${resource(dir: 'css/jQuery', file: 'jquery-ui-1.8.21.custom.css')}" type="text/css"/>
<link rel="stylesheet" href="${resource(dir: 'css/jQuery', file: 'jquery.multiselect.css')}" type="text/css"/>
<script type="text/javascript" src="${resource(dir: 'js/jQuery', file: 'jquery-1.7.2.min.js')}"></script>
<script type="text/javascript" src="${resource(dir: 'js/jQuery', file: 'jquery-ui-1.8.21.custom.min.js')}"></script>
<script type="text/javascript" src="${resource(dir: 'js/jQuery', file: 'jquery.ui.datepicker-es.js')}"></script>
<script type="text/javascript" src="${resource(dir: 'js/jQuery', file: 'jquery.multiselect.min.js')}"></script>
<div id="edit-tipoReunion" class="content scaffold-create">
  <h2>Editar tipo de reunión</h2>
  <g:hasErrors bean="${tipoReunionInstance}">
    <ul class="errors" role="alert">
      <g:eachError bean="${tipoReunionInstance}" var="error">
        <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
      </g:eachError>
    </ul>
  </g:hasErrors>
  <g:form action="save" name="guardar">
    <g:render template="mostrar"/>
    <br>
    <fieldset class="buttons" style="width:750px">
      <g:actionSubmit class="save" action="update" value="Editar" />
      <g:hiddenField id="id" name="id" value="${tipoReunionInstance?.id}"/>
      <a href="${createLink(uri: '/tipoReunion/crear')}" class="cancel" aling="center"><g:message code="Cancelar"/></a>
    </fieldset>
  </g:form>
</div>