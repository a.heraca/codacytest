package ControlDeReuniones

class Acuerdo {

    String descripcionAcuerdo
    Date fechaCompromiso
    String estado

    Minuta minuta

    static hasMany = [usuario:Usuario]

    static constraints = {
        estado inList: ["Pendiente", "Parcial", "Realizado", "Delegado", "Cancelado"]
    }
    
    String toString(){
        return descripcionAcuerdo
    }
    
}
