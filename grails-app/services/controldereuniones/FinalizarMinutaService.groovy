package ControlDeReuniones

class FinalizarMinutaService {

   boolean transactional=false
    
    //Declaración del método mailService pera el envio del correo. 
    def mailService
    
    def notificarMinuta(correoResponsable,final2,identificador,responsable,lugar,objetivo,tipoRInstance,fechaInicio,horaInicio,fechaFin,horaFin,usuarioInstance,punto,descripcionN,usAcuerdo,fechaCom,estadoM,miAcuerdo){
        
        mailService.sendMail {
            to final2
            from correoResponsable
            subject "Informacion de " + identificador
            //html (view:"/minuta/informacion", model:[identificador:identificador])
            html '<table align="center" width="700px" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:12px;"><th style="background:#585858; color:white;" height="40" colspan="4">Identificador:&nbsp;'+identificador+'</th>'+
            "\n"+'<tr align="left"> <th style="background:#585858; color:white;">Responsable:</th> <td style="background:#D8D8D8;">'+responsable+'</td> <th style="background:#585858; color:white;" height="30">Lugar:</th><td style="background:#D8D8D8;">'+lugar+'</td></tr>'+
            "\n"+'<tr align="left"><th style="background:#585858; color:white;" >Tipo de reunión:</th> <td style="background:#D8D8D8;">'+tipoRInstance+'</td><th style="background:#585858; color:white;" height="30">Objetivo:</th> <td style="background:#D8D8D8;">'+objetivo+'</td></tr>'+
            "\n"+'<tr align="left"><th style="background:#585858; color:white;" >Fecha inicio:</th> <td style="background:#D8D8D8;">'+fechaInicio+'</td><th style="background:#585858; color:white;" height="30">Hora inicio:</th><td style="background:#D8D8D8;">'+horaInicio+'</td></tr>'+
            "\n"+'<tr align="left"><th style="background:#585858; color:white;"> Fecha fin:</th><td style="background:#D8D8D8;">'+fechaFin+'</td><th style="background:#585858;color:white;" height="30">Hora fin:</th><td style="background:#D8D8D8;">'+horaFin+'</td></tr></table>'+
            "\n"+'<table align="center" width="700px" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:12px;" ><tr><td height="10" colspan="2"></td></tr><tr style="background:#585858; color:white;"><th height="40" colspan="4">Participantes</th></tr><tr ><td colspan="4" style="background:#D8D8D8;">'+usuarioInstance+'</td></tr></table>'+
            "\n"+'<table align="center" width="700px" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:12px;" ><tr><td height="10" colspan="2"></td></tr><tr style="background:#585858; color:white;"><th height="40" colspan="4">Puntos a tratar</th></tr><td style="background:#D8D8D8;">'+punto+'</td></tr></table>'+
            "\n"+'<table align="center"  style="text-align:center;" width="700px" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:12px;"><tr><td height="10" colspan="2"></td></tr><tr style="background:#585858; color:white;"><th height="40" colspan="4" style="background:#585858;">Acuerdos</th></tr>'+
            "\n"+'<tr align="center"><th style="background:#585858; color:white;" height="20">Descripción:</th><th style="background:#585858; color:white;" height="20">Responsable:</th><th style="background:#585858; color:white;" height="20">Fecha compromiso:</th><th style="background:#585858; color:white;" height="20">Estado:</th></tr>'+
            "\n"+miAcuerdo+'</table>'  
        }
    }
}
