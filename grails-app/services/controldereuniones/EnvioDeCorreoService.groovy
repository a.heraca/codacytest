package ControlDeReuniones

class EnvioDeCorreoService {

     boolean transactional=false
    
    //Declaración del método mailService pera el envio del correo. 
    def mailService
        
     def notificarCancelarMinuta(identificador, correoElectronico, correoRespo){        
         mailService.sendMail {
            to correoElectronico
            from correoRespo
            subject "La minuta con identificador " + identificador + " ha sido cancelada"
            body "Se les informa que que la Minuta con identificador '" + identificador + "' y sus acuerdos han sido cancelados. Sin mas por el momento, " +
            "les envío un coordial saludo."
        }
    }
}
