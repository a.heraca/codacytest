package controldereuniones

class CorreoUsuarioService {

    boolean transactional=false
    
    //Declaración del método mailService pera el envio del correo. 
    def mailService
        
     def envioCorreo(correoElectronico, usuario, nombre,  puesto, contraseña, correoRespo){        
         mailService.sendMail {
            to correoElectronico
            from correoRespo
            subject "La Informacion de sus datos ha sido modificada"
//            body "Se le informa que su in formacion ha sido cambiada verifique los datos " +
//                "Nombre: " + nombre+ "| Nombre de usuario: "+ usuario +" | Correo Electronico: "+ correoElectronico +" | Puesto: " + puesto +" Contraseña: " + contraseña
       html '<p>Se le informa que se ha modificado su informaci&oacute;n, quedando de la siguiente manera:</p><table align="center" width=700px><th style="background:#585858; color:white;" height="40" colspan="2">Datos Personales:</th>'+"\n"+
       '<tr align="left"><th height="30">Nombre del participante:</th><td align="center"><b>'+ nombre + '</b></td></tr>' + "\n" +
        '<tr align="left"><th height="30">Puesto:</th><td align="center"><b>' + puesto + '</b></td></tr>' + "\n" + 
            '<tr align="left"><th height="30">Usuario:</th><td align="center"><b>' + usuario + '</b></td></tr>' + "\n" + 
            '<tr align="left"><th height="30">Contrase&ntilde;a:</th><td align="center"><b>' + contraseña + '</b></td></tr>' + "\n" +
            '<tr align="left"><th height="30">Correo Electronico:</th><td align="center"><b>' + correoElectronico + '</b></td></tr>' + "\n" +
            '</table>'
        }
    }
}
//#585858