package ControlDeReuniones

class CorreoNotificacionAcuerdoService {

     boolean transactional=false
    
    //Declaración del método mailService pera el envio del correo. 
    def mailService
    
    def enviarCorreo(correoElectronico, asunto, mensaje, correoResponsable){
        mailService.sendMail {
            to correoElectronico
            from correoResponsable 
            subject asunto
            body mensaje
        }
    }
}
